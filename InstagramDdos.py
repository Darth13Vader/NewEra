from InstagramAPI import InstagramAPI

username = ''
password = ''
target_username = ''

api = InstagramAPI(username, password)
api.login()

api.fbUserSearch(target_username)
tagret_id = api.LastJson['users'][0]['user']['pk']

if api.getUserFeed(tagret_id):
    print('=' * 50)
    print(f"Starting liking {target_username}'s media...")
    allMedia = api.LastJson['items']
    total = 0
    mediaCount = len(allMedia)
    for media in allMedia:
        media_id = media['pk']
        if media['has_liked']:
            print(f'Media {media_id} already liked')
        else:
            if api.like(media_id):
                print(f'Media {media_id} was liked')
                total += 1
    print('-' * 50)
    print(f'Liked {total}/{mediaCount} media')
    print('=' * 50)
    print()
else:
    print("Unable to get user's feed")
